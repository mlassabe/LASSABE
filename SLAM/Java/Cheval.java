//Classe Cheval - Manu LASSABE
public class Cheval extends Animal{
	private String Couleur;
	
	
	
	public Cheval(String n, String t, double p, String c){
		super(n, t, p);
		this.Couleur=c;
		
		
	}

	public String getCouleur() {
		return Couleur;
	}

	public void setCouleur(String couleur) {
		this.Couleur = Couleur;
	}
	
	
	
	@Override
	void crie(){
		System.out.println("Je hennis");
	}
	
	void courir(){
		System.out.println("Je suis un cheval et je cours");
	}

	
}
