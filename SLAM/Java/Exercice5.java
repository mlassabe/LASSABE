import java.util.Scanner;

public class Exercice5 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		double tab2[][]= new double[3][20];
		
		for ( int i=0 ; i<3 ; i++){
			for (int j=0 ; j<20 ; j++){
				System.out.println("Contrôle n° "+(i+1)+" pour l'élève "+(j+1));
				double b= sc.nextDouble();
				tab2[i][j]=b;
			}
			System.out.println(" ");
		}
		System.out.println("Resultats des contrôles des élèves :");
		System.out.println("---------------------------------------------------------");
		for ( int i=0 ; i<3 ; i++){
			for (int j=0 ; j<20 ; j++){
				System.out.print(tab2[i][j]+" | ");
			}
			System.out.println(" ");
		}
		System.out.println(" ");
		System.out.println("Moyennes des élèves :");
		System.out.println("---------------------------------------------------------");
		for (int j=0 ; j<20 ; j++){
			double moyenne=0;
			for (int i=0 ; i<3 ; i++){
				moyenne+=tab2[i][j];
			}
			moyenne=moyenne/3;
			System.out.println("Moyenne pour l'élève "+(j+1) + " : "+moyenne);
		}
	}

}
