package LesVetements;


public class Pantalon extends Vetements {

	private double LongueurJambe;
	private double tourtaille;

	
	
	public Pantalon(String l, String c, String m, String n, double p, double j, double t){
		super(l, c, m, n, p);
		this.LongueurJambe=j;
		this.tourtaille=t;
		
	}
	
	public double getPantalon(double j, double t){
		return this.LongueurJambe;
		
	}
	
	
	
	public double getTourTaille() {
		return tourtaille;
	}
	
	

	public void setTourtaille(double tourtaille) {
		this.tourtaille = tourtaille;
	}
	
	
	public void setPantalon(double j, double t){
		this.LongueurJambe=j;
		this.tourtaille=t;
		
	}
	
	

	public double getLongueurJambe() {
		return LongueurJambe;
	}
	
	

	public void setLongueurJambe(double LongueurJambe) {
		this.LongueurJambe = LongueurJambe;
	}
	
	
	
	public static void main (String[]args){
		Pantalon pantalon1 = new Pantalon ("Pantalon", "Noir", "Bryce", "Bouled", 16.5, 40, 50);
		System.out.println(pantalon1);
		Pantalon pantalon2 = new Pantalon ("Pantalon", "Rouge", "Jules", "Ravage", 39.9, 50, 15);
		System.out.println(pantalon2);
		Pantalon pantalon3 = new Pantalon ("Pantalon", "Noir", "Armand Thierry", "Promps", 29.99, 60, 54);
		System.out.println(pantalon3);
		
	}
	
	
}