import java.util.Scanner;
public class Exercice4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String tab[][]={{"Mois","Août","Septembre","Octobre","Novembre","Decembre"},{"Gazole","1.42","1.38","1.35","1.25","1.26"},{"SP95","1.45","1.48","1.36","1.42","1.47"},{"SP98","1.44","1.47","1.35","1.41","1.46"},{"GPL","0.97","1.01","0.99","1.02","0.95"}};
		System.out.println("Historique des carburants :");
		System.out.println("-----------------------------------");
		for ( int i=0 ; i<6 ; i++){
			for (int j=0 ; j<5 ; j++){
				System.out.print(tab[j][i]+" | ");
			}
			System.out.println(" ");
		}

	}

}