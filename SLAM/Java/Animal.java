//Classe Animal - Manu LASSABE

public abstract class Animal {
    	
	private String nom;
	private String type;
	private double poids;
	



	public Animal(String n, String t, double p){
		this.nom = n;
		this.type = t;
		this.poids = p;
		
		

	}



	public String getNom() {
		return nom;
	}

	

	public void setNom(String n) {
		this.nom=n;
	}
	
	public String getType() {
		return type;
	}

	

	public void setType(String t) {
		this.type=t;
	}
	
	
	public double getPoids() {
		return poids;
	}

	

	public void setPoids(double p) {
		this.poids=p;
	}



	abstract void crie();
	
	void courir(){
		System.out.println("je suis un animal et je cours");
	}
		
	
	public static void main (String[]args){
		Zebre zebre1 = new Zebre ("Trotro", "Zebre", 100, 11);
		System.out.println(zebre1.toString());
		zebre1.crie();
		zebre1.courir();
		Zebre zebre2 = new Zebre ("Zebulon", "Aqua zebra", 100, 6);
		System.out.println(zebre2.toString());
		zebre2.crie();
		zebre2.courir();
		Zebre zebre3 = new Zebre ("Margott", "Zèbre d'Afrique", 100, 9);
		System.out.println(zebre3.toString());
		zebre3.crie();
		zebre3.courir();
		Cheval cheval1 = new Cheval ("Yakari", "Cheval de MLProd", 100, "Noir");
		System.out.println(cheval1.toString());
		cheval1.crie();
		cheval1.courir();
		Cheval cheval2 = new Cheval ("Trolololol", "Cheval de course", 100, "Gris");
		System.out.println(cheval2.toString());
		cheval2.crie();
		cheval2.courir();
		Cheval cheval3 = new Cheval ("Tornado", "Cheval Blanc", 100, "Blanc");
		System.out.println(cheval3.toString());
		cheval3.crie();
		cheval3.courir();
}
	public double vitesse(double valeur){
		return vitesse(valeur);
	}
	
	public int vitesse(int valeur){
		return vitesse(valeur);
	}
	
	
	 public String toString(){
		    String str = "Je m'appelle" + this.getNom() + ", je suis de type " + this.type + ", je pese " + this.poids;
		    return str;
		  } 
}