package LesVetements;



public class Vetements {

	private String libelle;
	private String couleur;
	private double prix;
	private String marque;
	private String collection;
	private compo compo;
	private int affichevete;
	static int nbVete = 0;

	

	public String getLibelle() {
		return libelle;
	}

	

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	

	public String getCouleur() {
		return couleur;
	}

	

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	

	public String getMarque() {
		return marque;
	}

	

	public void setMarque(String marque) {
		this.marque = marque;
	}

	

	public String getCollection() {
		return collection;
	}

	

	public void setCollection(String collection) {
		this.collection = collection;
	}

	

	public compo getCompo() {
		return compo;
	}

	

	public void setCompo(compo compo) {
		this.compo = compo;
	}

	

	public int getAffichevete() {
		return affichevete;
	}

	

	public void setAffichevete(int affichevete) {
		this.affichevete = affichevete;
	}

	

	public static int getNbVete() {
		return nbVete;
	}

	

	public static void setNbVete(int nbVete) {
		Vete.nbVete = nbVete;
	}

	

	public Vete(String l, String c, String m, String n, double p){
		this.libelle = l;
		this.couleur = c;
		this.marque = m;
		this.prix = p;
		this.collection = n;
		this.compo = null;

	}

	

	public int getVete(){
		return this.affichevete;
	}

	

	public void setVete(int h){
		this.affichevete=h;
	}

	

	public void AfficheVetements(){
		System.out.println(libelle + couleur + prix + marque + collection);
		if(this.compo != null){
			this.compo.afficheComposant();
		}

		
	}

	public double getPrix() {
		return prix;
	}

	

	public void setPrix(double prix) {
		this.prix = prix;
	}

	

	public String toString(){
		Vete.nbVete ++;
		return getLibelle()+" "+Vetements.nbVete+", "+getCouleur()+", "+getMarque()+", "+getPrix()+" euros, "+getCollection()+".";	
	}

	

	public static void main(String[]args){
		compo coton1 = new compo("coton ", 30, " chine", 40);
		coton1.afficheComposant();
		Vete pantalon1 = new Vete("pantalon", " rouge", " Wiwi", " Nido", 10);
		pantalon1.affichevete();
		pantalon1.compo=coton1;

	}

}