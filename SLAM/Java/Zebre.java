//Classe Zebre - Manu LASSABE
public class Zebre extends Animal{
	
	static int rayure = 10;
	
	
	
	public Zebre(String n, String t, double p, int rayure){
		super(n, t, p);
		
	}
	
	public int getRayure() {
		return Rayure;
	}

	public void setRayure(int rayure) {
		this.Rayure = Rayure;
	}
	
	
	
	
	void crie(){
		System.out.println("Je hennis");
	}
	
	
	void courir(){
		System.out.println("Je suis un zebre et je cours");
	}
	
	

	public String toString(){
		return super.toString()+ " J'ai "+this.getRayure()+" rayures";
	}



}
