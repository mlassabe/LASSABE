#Manu LASSABE
#17/10/2016
#TD4 : Structures iteratives
#Ex 2 : Sommes de puissances avec des grains de riz
u=0
k = int(input("Combien de cases dans l'échéquier ? "))
p = int(input("Combien de grains de riz dans un kilo ? "))
print("Le roi doit déposer un grain de riz sur chaque case de l'échéquier")
print("Sachant qu'un échéquier fait 64 cases et qu'il dépose un grain de riz")
print("à la premiere case")
for i in range(k):
    v=2**i
    u=u+v
    i+=1
    if i in range (1,11) :
        reponse = input("Voulez-vous connaitre la réponse pour la somme !? ")
        if reponse == "oui" :
            print(u)
        else :
            print("Pas Curieux !")
    else :
        print(u)
            
print ("Le roi doit déposer au total ",u, " grains de riz")
print ("ce qui représente ",u/p," kilos de riz !") 
