#TD.algorithme
#Emmanuel LASSABE
 


from turtle import *
p=-240
o=-240
up()
goto(o,p)
down()

#fenêtre principale:
for i in range(4):
	forward(480)
	left(90)

#faire les 4 lignes qui commence par un carreau noire:
for i in range(4):
	#faire une ligne commençant par une case noire:
	for i in range(4):
		begin_fill()
		#faire un carré
		for i in range(4):
			forward(60)
			left(90)
		end_fill()
		forward(120)
	p=p+120
	up()
	goto(o,p)
	down()

#faire les 4 lignes à case blanche:
up()
p=-180
goto(o,p)
down()
for i in range(4):
	for i in range(4):
		forward(120)
		begin_fill()
		for i in range (4):
			left(90)
			forward(60)
		end_fill()
	p=p+120
	up()
	goto(o,p)
	down()
exitonclick()




