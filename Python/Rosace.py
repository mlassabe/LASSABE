from turtle import *
#Construction Rosace

# Fonctions
def cercle(rayon,x,y):
    up()
    goto(x,y-rayon)
    down()
    circle(rayon)

def helice(rayon,couleur):
        color(couleur)
        begin_fill()
        for i in range(3):
            left(60)
            circle(rayon,120)
            left(60)
        end_fill()
        color('black')

def helice2(rayon,couleur2):
        color(couleur2)
        begin_fill()
        for i in range(3):
            left(60)
            circle(rayon,120)
            left(60)
        end_fill()
        color('black')
        
def avancersurcercle(rayon):
    circle(rayon,60)

def avancersurcercle2(rayon):
    circle(rayon,30)
    
def rosace(rayon,x,y,couleur,couleur2):
    cercle(rayon,x,y)
    helice(rayon,couleur)
    avancersurcercle(rayon)
    helice(rayon,couleur)
    avancersurcercle2(rayon)
    helice2(rayon,couleur2)
    avancersurcercle(rayon)
    helice2(rayon,couleur2)
    left(210)




#Programme principal

couleur =input("Quelle couleur choisis-tu : ")
couleur2 =input("Quelle couleur choisis-tu : ")
rosace(200,0,0,couleur,couleur2)
rosace(30,200,-200,couleur2,couleur)
rosace(30,200,200,couleur2,couleur)
rosace(30,-200,200,couleur2,couleur)
rosace(30,-200,-200,couleur2,couleur)
hideturtle()
exitonclick()


