-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 22, 2016 at 04:59 PM
-- Server version: 5.5.49-0+deb8u1
-- PHP Version: 5.6.20-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lassae`
-- Database de TD Veterinare

-- --------------------------------------------------------

--
-- Table structure for table `animal`
--

CREATE TABLE IF NOT EXISTS `animal` (
`numAnimal` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `animal`
--

INSERT INTO `animal` (`numAnimal`, `nom`) VALUES
(1, 'Pompom'),
(2, 'Pomponnette'),
(3, 'Jerry'),
(5, 'Azrael');

-- --------------------------------------------------------

--
-- Table structure for table `animal2`
--

CREATE TABLE IF NOT EXISTS `animal2` (
`numAnimal` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `animal2`
--

INSERT INTO `animal2` (`numAnimal`, `nom`) VALUES
(1, 'Pompom'),
(2, 'Pomponnette'),
(3, 'Jerry'),
(4, 'Snoopy'),
(5, 'Azrael');

-- --------------------------------------------------------

--
-- Table structure for table `consultation`
--

CREATE TABLE IF NOT EXISTS `consultation` (
  `numeroProprio` int(11) NOT NULL,
  `numeroAnimal` int(11) NOT NULL,
  `dateConsultation` date NOT NULL,
  `raison` varchar(20) NOT NULL,
  CONSTRAINT fk_PrenomMedecin FOREIGN KEY (PrenomVeterinaire) REFERENCES Medecin(PrenomMedecin)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultation`
--

INSERT INTO `consultation` (`numeroProprio`, `numeroAnimal`, `dateConsultation`, `raison`, `prenomMedecin`) VALUES
(1, 1, '2013-09-15', 'Renal', 'Gabrielle'),
(1, 2, '2013-09-20', 'Vaccin', 'Gabrielle'),
(2, 3, '2013-09-03', 'Blessure', 'Gabrielle'),
(3, 4, '2013-09-10', 'Troubles du sommeil', 'Pierre'),
(4, 5, '2013-09-01', 'Probelemes comportem', 'Pierre');

-- --------------------------------------------------------

--
-- Table structure for table `consultation2`
--

CREATE TABLE IF NOT EXISTS `consultation2` (
`numeroC` int(11) NOT NULL,
  `numeroProprio` int(11) NOT NULL,
  `numeroAnimal` int(11) NOT NULL,
  `dateConsultation` date NOT NULL,
  `raison` varchar(20) NOT NULL,
  CONSTRAINT fk_PrenomMedecin FOREIGN KEY (PrenomVeterinaire) REFERENCES Medecin(PrenomMedecin)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultation2`
--

INSERT INTO `consultation2` (`numeroC`, `numeroProprio`, `numeroAnimal`, `dateConsultation`, `raison`, `prenomMedecin`) VALUES
(1, 1, 1, '2013-09-15', 'Renal', 'Gabrielle'),
(2, 1, 2, '2013-09-20', 'Vaccin', 'Gabrielle'),
(3, 2, 3, '2013-09-03', 'Blessure', 'Gabrielle'),
(4, 3, 4, '2013-09-10', 'Troubles du sommeil', 'Pierre'),
(5, 4, 5, '2013-09-01', 'Probelemes comportem', 'Pierre');

-- --------------------------------------------------------

--
-- Table structure for table `Medecin`
--

CREATE TABLE IF NOT EXISTS `Medecin` (
  `NumM` int(11) NOT NULL DEFAULT '0',
  `NomMedecin` char(20) DEFAULT NULL,
  `PrenomMedecin` char(20) DEFAULT NULL,
  `Numtel` char(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Medecin`
--

INSERT INTO `Medecin` (`NumM`, `NomMedecin`, `PrenomMedecin`, `Numtel`) VALUES
(1, 'Burel', 'Pierre', '0123654789'),
(2, 'Burel', 'Gabrielle', '0123456987');

-- --------------------------------------------------------

--
-- Table structure for table `proprietaire`
--

CREATE TABLE IF NOT EXISTS `proprietaire` (
`numProprio` int(11) NOT NULL,
  `nomProprio` varchar(20) NOT NULL,
  `prenomProprio` varchar(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proprietaire`
--

INSERT INTO `proprietaire` (`numProprio`, `nomProprio`, `prenomProprio`) VALUES
(1, 'Castagnier', 'Aimable'),
(2, 'Aimée', 'Madeleine'),
(3, 'Brown', 'Charly'),
(4, 'Gargamel', 'Jules');

-- --------------------------------------------------------

--
-- Table structure for table `proprietaire2`
--

CREATE TABLE IF NOT EXISTS `proprietaire2` (
`numProprio` int(11) NOT NULL,
  `nomProprio` varchar(20) NOT NULL,
  `prenomProprio` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proprietaire2`
--

INSERT INTO `proprietaire2` (`numProprio`, `nomProprio`, `prenomProprio`) VALUES
(1, 'Castagnier', 'Aimable'),
(2, 'Aimée', 'Madeleine'),
(3, 'Brown', 'Charly'),
(4, 'Gargamel', 'Jules');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `animal`
--
ALTER TABLE `animal`
 ADD PRIMARY KEY (`numAnimal`);

--
-- Indexes for table `animal2`
--
ALTER TABLE `animal2`
 ADD PRIMARY KEY (`numAnimal`);

--
-- Indexes for table `consultation`
--
ALTER TABLE `consultation`
 ADD PRIMARY KEY (`numeroProprio`,`numeroAnimal`), ADD KEY `consultation_ibfk_2` (`numeroAnimal`), ADD KEY `consultation_ibfk_` (`prenomMedecin`);

--
-- Indexes for table `consultation2`
--
ALTER TABLE `consultation2`
 ADD PRIMARY KEY (`numeroC`), ADD KEY `numeroAnimal` (`numeroAnimal`), ADD KEY `numeroProprio` (`numeroProprio`);

--
-- Indexes for table `Medecin`
--
ALTER TABLE `Medecin`
 ADD PRIMARY KEY (`NumM`);

--
-- Indexes for table `proprietaire`
--
ALTER TABLE `proprietaire`
 ADD PRIMARY KEY (`numProprio`);

--
-- Indexes for table `proprietaire2`
--
ALTER TABLE `proprietaire2`
 ADD PRIMARY KEY (`numProprio`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `animal`
--
ALTER TABLE `animal`
MODIFY `numAnimal` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `animal2`
--
ALTER TABLE `animal2`
MODIFY `numAnimal` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `consultation2`
--
ALTER TABLE `consultation2`
MODIFY `numeroC` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `proprietaire`
--
ALTER TABLE `proprietaire`
MODIFY `numProprio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `proprietaire2`
--
ALTER TABLE `proprietaire2`
MODIFY `numProprio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `consultation2`
--
ALTER TABLE `consultation2`
ADD CONSTRAINT `consultation2_ibfk_2` FOREIGN KEY (`numeroAnimal`) REFERENCES `animal2` (`numAnimal`),
ADD CONSTRAINT `consultation2_ibfk_1` FOREIGN KEY (`numeroProprio`) REFERENCES `proprietaire2` (`numProprio`);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
