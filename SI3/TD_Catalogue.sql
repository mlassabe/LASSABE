



-- --------------------------------------------------------


--
-- Structure de la table `article`
--


CREATE TABLE `article` (
  `Refprod` varchar(10) NOT NULL,
  `désignation` varchar(20) NOT NULL,
  `Couleur` varchar(10) DEFAULT NULL,
  `Dimension` varchar(10) DEFAULT NULL,
  `PRIX HT` float DEFAULT NULL,
  `NumColl` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


--
-- Contenu de la table `article`
--


INSERT INTO `article` (`Refprod`, `désignation`, `Couleur`, `Dimension`, `PRIX HT`, `NumColl`) VALUES
('A12', 'Chaise longue', 'Marine', '120*60', 90, '1'),
('A14', 'Serviette de bain', 'Orangé', '130*80', 65, ' 2'),
('A15', 'Coussin', 'Paille', '30*30', 12, ' 2');


-- --------------------------------------------------------


--
-- Structure de la table `collection`
--


CREATE TABLE `collection` (
  `NumColl` varchar(10) NOT NULL,
  `Datelancement` varchar(20) DEFAULT NULL,
  `Harmonie` varchar(10) DEFAULT NULL,
  `NomColl` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


--
-- Contenu de la table `collection`
--


INSERT INTO `collection` (`NumColl`, `Datelancement`, `Harmonie`, `NomColl`) VALUES
('1', '2005/04/01', 'Bleu', 'Marée Haute '),
('2', '2005/04/15', 'Jaune', 'Soleil'),
(' 2', '2005/04/15', 'Jaune', '  Soleil'),
(' 1', '2005/04/01', 'Bleu', ' Marée Haute '),
('   2', '2005/04/15', 'Jaune', ' Soleil'),
('3', '2011/10/18', 'rouge', 'Vent Pourpre');


--
-- Index pour les tables exportées
--


--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`Refprod`),
  ADD KEY `NumColl` (`NumColl`);


--
-- Index pour la table `collection`
--
ALTER TABLE `collection`
  ADD PRIMARY KEY (`NumColl`);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;