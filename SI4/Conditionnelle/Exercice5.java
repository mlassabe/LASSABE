import java.util.Scanner ;
public class Exercice5 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Saisis un nombre (negatif ou positif) : ");
		double nombre1 = sc.nextInt();
		System.out.println("Saisis un nombre (negatif ou positif) : ");
		double nombre2 = sc.nextInt();
		if (nombre1 == nombre2 ){                                      //On effectue un test et il est bool�en
			System.out.println("Ces nombres sont identiques");         //Le test est positif, on effectue une action
		}
		else {
			System.out.println("Ces nombres sont diff�rents");         //Le test est negatif, on effectue une autre action
		}

	}

}
