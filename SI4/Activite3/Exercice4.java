import java.util.Scanner;
public class Exercice4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Veuillez entrer un nombre entre 1 et 3 : ");
		int nombre = sc.nextInt();
		while (nombre<1 || nombre>3){
			System.out.print("Erreur ! Veuillez entrer un nombre entre 1 et 3 : ");
			nombre=sc.nextInt();
		}
		System.out.println("Bravo !");
	}

}
