import java.util.Scanner;

public class Exercice5 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Entrez l'age : ");
		int age = sc.nextInt();
		if (age==6 || age==7 ){
			System.out.println("Poussin");
		}
		else if (age==8 || age==9 ){
			System.out.println("Pupille");
		}
		else if (age==10 || age==11 ){
			System.out.println("Mimime");
		}
		else if (age>=12){
			System.out.println("Cadet");
		}
	}
}