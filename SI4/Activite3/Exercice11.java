import java.util.Scanner;
public class Exercice11 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Veuillez entrer OUI ou NON : ");
		char nombre=sc.next().charAt(0);
		while (nombre!='o' && nombre!='n'){
			System.out.print("Erreur ! Veuillez entrer OUI ou NON : ");
			nombre=sc.next().charAt(0);
		}
		System.out.print("Bravo !");
	}
}

