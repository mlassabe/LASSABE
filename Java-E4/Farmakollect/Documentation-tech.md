# Documentation d'utilisation

Ce document est une documentation d'utilisation de l'application *Farmakollect* 

## Accès à l'application

L'accès à l'application n'est accessible qu'en local pour le moment 

### Identifiants de connexion

Des identifiants de connexion ont étés crées et donnent accès aux différents modules de l'application, on dénomme ainsi les accès:
- *Administrateur*, accès à tous les modules de l'application ;
- *Pharmacien Hospitalier / Officine*, accès aux articles et au module d'espace de commande ;
- *Personnel de santé / Médecin*, pour ce type d'utilisateur, accès au détail des articles pas encore éxécuté

Ci-dessous voici différents identifiants afin d'accéder à la plateforme:

| Accès          | Login         | Mot de passe  |
| -------------  |:-------------:| --------------|
| Administrateur | MLProd        | superadmin    |
| Pharmacien Hospitalier | elass | superadmin    | 
| Pharmacien Officine | alass        | root   |
| Personnel de santé | FreLu      | root    |
| Médecin | FLMeur | root    |


## Fonctionnalités


### Ajout d'un article à la vente

*Accès:* Administrateur
* Depuis la barre de navigation, se rendre dans l'espace administrateur puis dans ajouter un produit.
* *N.B:* Le code assigné à un produit ne peut pas être changé.

### Gestion d'un produit

*Accès:* Administrateur
* Depuis l'espace administrateur, éditer ou supprimer un produit présent dans la liste des produits recensés dans la colone "action".

### Ajout d'un utilisateur

*Accès:* Administrateur
* Depuis la barre de navigation, se rendre dans l'espace administrateur puis dans ajouter un utilisateur.
* *N.B* Si l'établissement médical de l'utilisateur n'est pas renseigné, il est alors nécessaire de le créer dans l'espace de gestion des organismes médicaux.

### Ajout d'un établissement

*Accès:* Administrateur
* Depuis la barre de navigation, se rendre dans l'espace administrateur puis dans celui des organismes médicaux.


## Développement du projet

### Modules fonctionnels

- [x] Module de gestion de médicaments
- [x] Module de gestion des établissements
- [x] Module de gestion des utilisateurs


### Non traités

Certaines fonctionnalités de l'API ne sont pas incluses:
- L'édition, la suppression d'un utilisateur ;
- L'édition et la suppression d'un établissement ;
- Tout ce qui concerne ls commandes (ajout, validation , suppression, édition)