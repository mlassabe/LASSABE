<?php
require 'slim/vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App();

/*getConnection*/

function getConnection(){
    $bdd = new PDO('mysql:host=localhost;dbname=asfootball;charset=utf8', 'root', '');
    return $bdd;
}

/*URI et méthodes pour Personnes*/

$app->get("/list_personne", function(Request $request, Response $response){
    $bdd = new PDO('mysql:host=localhost;dbname=asfootball;charset=utf8', 'root', '');
    $sql=('SELECT * FROM personne ORDER BY id_personne');
    $req=$bdd->prepare($sql);
    $req->execute();
    $ttpersonne=$req->fetchAll(PDO::FETCH_ASSOC);
    $ret=json_encode($ttpersonne, JSON_PRETTY_PRINT);
    return $ret;   
});

$app->get("/personne/{id}", function(Request $request, Response $response){
    $id = $request->getAttribute('id');
    $sql=getConnection()->prepare('SELECT * FROM personne WHERE id_personne = :id');
    $sql->bindParam(':id', $id);
    $sql->execute();
    $personne=$sql->fetch(PDO::FETCH_OBJ);
    $response->getBody()->write(json_encode($personne, JSON_PRETTY_PRINT));
    return $response;
});

$app->post('/personneAjout', function(Request $request){ 
    $nompers = $request->getQueryParam('nom_personne'); 
    $prenompers = $request->getQueryParam('prenom_personne');
    $telPortable = $request->getQueryParam('telPortable_personne');
    $telFixe = $request->getQueryParam('telFixe_personne');
    $adresse = $request->getQueryParam('adresse_personne');
    $cp = $request->getQueryParam('codePostal_personne');
    $ville = $request->getQueryParam('ville_personne');
    $bdd = new PDO('mysql:host=localhost;dbname=asfootball;charset=utf8', 'root', '');
    $sql = ('INSERT INTO personne (nom_personne,prenom_personne,telPortable_personne,telFixe_personne,adresse_personne,codePostal_personne,ville_personne) VALUES (?,?,?,?,?,?,?)');
    $stmt = $bdd->prepare($sql); 
    $stmt->execute(array($nompers, $prenompers, $telPortable, $telFixe, $adresse, $cp, $ville));
   });

   $app->post('/personneEdit/{id}', function(Request $request){ 
    $id = $request->getAttribute('id');
    $personne=json_decode($request->getBody());
    $sql = getConnection()->prepare('UPDATE personne 
    SET nom_personne = :nompers, 
    prenom_personne = :prenompers,
    telPortable_personne = :telPortable,
    telFixe_personne = :telFixe,
    adresse_personne = :adresse,
    codePostal_personne = :codePostal,
    ville_personne = :ville 
    WHERE id_personne = :id'); 
    $sql->bindParam(":nompers",$personne->nompers);
    $sql->bindParam(":prenompers",$personne->prenompers);
    $sql->bindParam(":telPortable",$personne->telPortable);
    $sql->bindParam(":telFixe",$personne->telFixe);
    $sql->bindParam(":adresse",$personne->adresse);
    $sql->bindParam(":codePostal",$personne->cp);
    $sql->bindParam(":ville",$personne->ville);
    $sql->bindParam(":id",$id);
    $sql->execute();
    $sql->closeCursor();
   });

   $app->delete('/deletePersonne/{id}', function(Request $request){ 
    $id = $request->getAttribute('id');
    $stmt = getConnection()->prepare('DELETE * FROM personne WHERE id_personne=:id'); 
    $stmt->bindParam(":id",$id);
    $stmt->execute();
   });

/*URI et méthodes pour joueurs*/

$app->get("/list_joueur", function(Request $request, Response $response){
    $bdd = new PDO('mysql:host=localhost;dbname=asfootball;charset=utf8', 'root', '');
    $sql=('SELECT id_joueur, personne.nom_personne, personne.prenom_personne, joueur.mail1_joueur, joueur.mail2_joueur, id_pere, id_mere FROM personne INNER JOIN joueur ON personne.id_personne=joueur.id_personne ');
    $req=$bdd->prepare($sql);
    $req->execute();
    $ttlicence=$req->fetchAll(PDO::FETCH_ASSOC);
    $ret=json_encode($ttlicence, JSON_PRETTY_PRINT);
    return $ret;   
});

$app->get("/joueur/{id}", function(Request $request, Response $response){
    $id = $request->getAttribute('id_joueur');
    $bdd = new PDO('mysql:host=localhost;dbname=asfootball;charset=utf8', 'root', '');
    $sql=('SELECT id_joueur, personne.nom_personne, personne.prenom_personne, joueur.mail1_joueur, joueur.mail2_joueur, id_pere, id_mere FROM personne INNER JOIN joueur ON personne.id_personne=joueur.id_personne WHERE id_joueur=:id');
    $sql->bindParam(":id",$id,PDO::PARAM_INT);
    $req=$bdd->prepare($sql);
    $req->execute();
    $ttlicence=$req->fetchAll(PDO::FETCH_ASSOC);
    $ret=json_encode($ttlicence, JSON_PRETTY_PRINT);
    return $ret;   
});

$app->post('/joueurAjout', function(Request $request){ 
    $mail1 = $request->getQueryParam('mail1_joueur'); 
    $mail2 = $request->getQueryParam('mail2_joueur');
    $idpere = $request->getQueryParam('id_pere');
    $idmere = $request->getQueryParam('id_mere');
    $idpersonne = $request->getQueryParam('id_personne');
    $bdd = new PDO('mysql:host=localhost;dbname=asfootball;charset=utf8', 'root', '');
    $sql = ('INSERT INTO joueur (mail1_joueur,mail2_joueur,id_pere,id_mere,id_personne) VALUES (?,?,?,?,?)');
    $stmt = $bdd->prepare($sql); 
    $stmt->execute(array($mail1, $mail2, $idpere, $idmere, $idpersonne));
   });

/*URI et méthodes pour Users*/

$app->get("/list_users", function(Request $request, Response $response){
    $bdd = new PDO('mysql:host=localhost;dbname=asfootball;charset=utf8', 'root', '');
    $sql=('SELECT privilege.libelle_privilege, id_utilisateur, login_utilisateur, mail_utilisateur FROM utilisateurs INNER JOIN privilege ON utilisateurs.id_privilege=privilege.id_privilege ');
    $req=$bdd->prepare($sql);
    $req->execute();
    $ttpersonne=$req->fetchAll(PDO::FETCH_ASSOC);
    $ret=json_encode($ttpersonne, JSON_PRETTY_PRINT);
    return $ret;   
});

$app->get("/user/{id}", function(Request $request, Response $response){
    $id = $request->getAttribute('id');
    $sql=getConnection()->prepare('SELECT * FROM utilisateurs WHERE id_utilisateur = :id');
    $sql->bindParam(':id', $id);
    $sql->execute();
    $user=$sql->fetch(PDO::FETCH_OBJ);
    $response->getBody()->write(json_encode($user, JSON_PRETTY_PRINT));
    return $response;
});

$app->post('/userAjout', function(Request $request){ 
    $login = $request->getQueryParam('login_utilisateur'); 
    $password = $request->getQueryParam('password_utilisateur');
    $mail = $request->getQueryParam('mail_utilisateur');
    $privilege = $request->getQueryParam('id_privilege');
    $bdd = new PDO('mysql:host=localhost;dbname=asfootball;charset=utf8', 'root', '');
    $sql = ('INSERT INTO utilisateurs (login_utilisateur,password_utilisateur,mail_utilisateur,id_privilege) VALUES (?,?,?,?)');
    $stmt = $bdd->prepare($sql); 
    $stmt->execute(array($login, $password, $mail, $privilege));
   });

   $app->post('/userEdit/{id}', function(Request $request){ 
    $id = $request->getAttribute('id');
    $user=json_decode($request->getBody());
    $sql = getConnection()->prepare('UPDATE utilisateurs 
    SET login_utilisateur = :login, 
    password_utilisateur = :password,
    mail_utilisateur = :mail,
    id_privilege = :privilege, 
    WHERE id_utilisateur = :id'); 
    $sql->bindParam(":login",$user->login);
    $sql->bindParam(":password",$user->password);
    $sql->bindParam(":mail",$user->mail);
    $sql->bindParam(":privilege",$user->privilege);
    $sql->bindParam(":id",$id);
    $sql->execute();
    $sql->closeCursor();
   });

/* URI et methodes pour Pere et Mere*/

$app->get("/list_mere", function(Request $request, Response $response){
    $bdd = new PDO('mysql:host=localhost;dbname=asfootball;charset=utf8', 'root', '');
    $sql=('SELECT personne.prenom_personne, mere.id_mere from mere INNER JOIN personne ON mere.id_personne=personne.id_personne ORDER BY mere.id_mere');
    $req=$bdd->prepare($sql);
    $req->execute();
    $ttlicence=$req->fetchAll(PDO::FETCH_ASSOC);
    $ret=json_encode($ttlicence, JSON_PRETTY_PRINT);
    return $ret;   
});

$app->get("/list_pere", function(Request $request, Response $response){
    $bdd = new PDO('mysql:host=localhost;dbname=asfootball;charset=utf8', 'root', '');
    $sql=('SELECT personne.prenom_personne, pere.id_pere from pere INNER JOIN personne ON pere.id_personne=personne.id_personne ORDER BY pere.id_pere');
    $req=$bdd->prepare($sql);
    $req->execute();
    $ttlicence=$req->fetchAll(PDO::FETCH_ASSOC);
    $ret=json_encode($ttlicence, JSON_PRETTY_PRINT);
    return $ret;   
});

   $app->post('/mereAjout', function(Request $request){ 
    $nommere = $request->getQueryParam('id_personne'); 
    $bdd = new PDO('mysql:host=localhost;dbname=asfootball;charset=utf8', 'root', '');
    $sql = ('INSERT INTO mere (id_personne) VALUES (?)');
    $stmt = $bdd->prepare($sql); 
    $stmt->execute(array($nommere));
   });

   $app->post('/pereAjout', function(Request $request){ 
    $nompere = $request->getQueryParam('id_personne'); 
    $bdd = new PDO('mysql:host=localhost;dbname=asfootball;charset=utf8', 'root', '');
    $sql = ('INSERT INTO pere (id_personne) VALUES (?)');
    $stmt = $bdd->prepare($sql); 
    $stmt->execute(array($nompere));
   });


/* URI et methodes pour licence */

$app->get("/list_licence", function(Request $request, Response $response){
    $bdd = new PDO('mysql:host=localhost;dbname=asfootball;charset=utf8', 'root', '');
    $sql=('SELECT licence.id_licence, licence.date_licence, licence.cat_licence, licence.dirigeant_licence ,personne.prenom_personne, saison.annee_saison FROM licence INNER JOIN saison ON licence.id_saison=saison.id_saison INNER JOIN joueur ON licence.id_joueur=joueur.id_joueur INNER JOIN personne ON personne.id_personne=joueur.id_personne ORDER BY date_licence');
    $req=$bdd->prepare($sql);
    $req->execute();
    $ttlicence=$req->fetchAll(PDO::FETCH_ASSOC);
    $ret=json_encode($ttlicence, JSON_PRETTY_PRINT);
    return $ret;   
});
/* URI et methodes pour privilege*/

$app->get("/list_privilege", function(Request $request, Response $response){
    $bdd = new PDO('mysql:host=localhost;dbname=asfootball;charset=utf8', 'root', '');
    $sql=('SELECT * FROM privilege ORDER BY id_privilege');
    $req=$bdd->prepare($sql);
    $req->execute();
    $ttpersonne=$req->fetchAll(PDO::FETCH_ASSOC);
    $ret=json_encode($ttpersonne, JSON_PRETTY_PRINT);
    return $ret;   
});



$app->run();




?>