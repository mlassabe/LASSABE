#######################################################
##                                                   ##
## Transformation Chiffres Arabes - Chiffres Romains ##
##               Manu LASSABE - BTS 1                ##
##                                                   ##
#######################################################
from tkinter import *
symb=['I','V','X','L','C','D','M','V*','X*','L*','C*']
#######################################################
##              Définition fonctions                 ##
#######################################################

def Romain(u):
    res=""
    u=int(u)
    if (u==9):
        res=symb[0]+symb[2]
    else:
        if(u>=5):
            res+=symb[1]
            u=u-5
        if(u==4):
            res=symb[0]+symb[1]
        else:
            for i in range(u):
                res+=symb[0]
    return res      


def symbolesqueeze(u):
    u.remove(u[0])
    u.remove(u[0])


def reponse():
    ad=entree.get()
    chaine="" 
    for i in range(len(ad)):
        c=ad[-1-i]
        chaine=Romain(c)+chaine
        symbolesqueeze(symb)
    canevas1.create_text(200,120,text="Le chiffre romain correspondant à",font="Arial 14 normal", fill="white")
    canevas1.create_text(200,140,text=ad,font="Arial 18 normal", fill="white")
    canevas1.create_text(200,160,text="est",font="Arial 18 normal", fill="white")
    canevas1.create_text(200,180,text=chaine,font="Arial 18 normal", fill="white")

    

#######################################################
##              Programme principal                  ##
#######################################################
encar = Tk()
canevas1 = Canvas(encar,bg='gray',height=300, width=400)
canevas1.pack()
mon_image = PhotoImage(file = 'manuscript1.gif')
item = canevas1.create_image(200,150,image = mon_image)
entree = Entry(encar, textvariable="", width=30)
entree.pack(side='left')
bouton1 = Button(encar, text= 'Rédiger', command = reponse)
bouton1.pack(side='right')
encar.mainloop()




    
