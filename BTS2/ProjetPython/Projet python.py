##########################################
##      Menu Python - Manu LASSABE      ##
##            Projet PYTHON             ##
##        Portefeuille numérique        ##
##########################################

################################################ IMPORTATIONS DES BIBLIOTHEQUES #####################################################################

from random import *
from tryalgo.arithm_expr_target import arithm_expr_target
from tkinter import *
import time
from datetime import datetime

################################################ FONCTIONS ##########################################################################################

##Pour le programme 1
def nbalea():
    L=[]
    A=[1,2,3,4,5,6,7,8,9,10,25,50,75,100]
    for j in range(6):
        NB=choice(A)
        L.append(NB)
    return L

def process(a,b):
    print("Voici les chiffres pour ce compte est Bon")
    print(a)
    time.sleep(10) ##Pause
    print("et le résultat à trouver",b)
    Calcul=arithm_expr_target(a,b)
    time.sleep(3) ##Pause
    print("C'est parti ! 30 secondes ")
    time.sleep(30)
    print("Le résultat est le suivant : ","\n",Calcul)
    

##Pour le programme 2
def TrianglePascal(n):
    for i in range(0,n+1):
        L2.append(1)
        for j in range(i-1,0,-1):
            L2[j]=L2[j]+L2[j-1]
    return L2

def expression(po,n):
    classe=""
    for i in range(n+1):
        if (classe!=""):
            classe=" + "+classe
        ty="{}".format(po[i])+"a**{} b**{} ".format(i,n-i)
        classe=ty+classe
    return classe

##Pour le programme 3
def Romain(u):
    res=""
    u=int(u)
    if (u==9):
        res=symb[0]+symb[2]
    else:
        if(u>=5):
            res+=symb[1]
            u=u-5
        if(u==4):
            res=symb[0]+symb[1]
        else:
            for i in range(u):
                res+=symb[0]
    return res      


def symbolesqueeze(u):
    u.remove(u[0])
    u.remove(u[0])

##Pour le programme 3 - Tkinter##
def reponse():
    ad=""
    ad=entree.get()
    chaine="" 
    for i in range(len(ad)):
        c=ad[-1-i]
        chaine=Romain(c)+chaine
        symbolesqueeze(symb)
    canevas1.create_text(200,120,text="Le chiffre romain correspondant à",font="Arial 14 normal", fill="white")
    canevas1.create_text(200,140,text=ad,font="Arial 18 normal", fill="white")
    canevas1.create_text(200,160,text="est",font="Arial 18 normal", fill="white")
    canevas1.create_text(200,180,text=chaine,font="Arial 18 normal", fill="white")

##Pour le programme 4
def occurence_dans_chaine(ch,c):
    i=0
    r=0
    for i in range(len(ch)):
        if ch[i]==c:
            r=r+1
    return r

##Pour le programme 5
def convNombre (nombre):
    n=len(nombre)
    r=[]
    u=len(r)
    p=(list(nombre))
    while (6-u<n):
        r.append(0)
        print (r)
        u+=1
    p=r+p
    for i in range (n-6,n-5):
        p[i]=listeu[int(nombre[i])]
        if (p[i]==" "):
            p[i]=''
        elif (p[i]=="un"):
            p[i]="cent"
        else :
            p[i]=p[i]+" cent"
    for i in range (n-5,n-4):
        p[i]=listedec[int(nombre[i])]
        if (p[i]=="dix"):
            p[i]=listed[int(nombre[i+1])]+" mille"
            p[i+1]=""
        elif (p[i]=="soixante-dix"):
            p[i]="soixante-"+listed[int(nombre[i+1])]+" mille"
            p[i+1]=""
            if (p[i+1]=="un"):
                p[i]="-et-"+listed[int(nombre[i+1])]+" mille"
        elif (p[i]=="quatre-vingt-dix"):
            if (p[i+1]=="un"):
                p[i]="quatre-vingt-et-"+listed[int(nombre[i])]+" mille"
            else :
                p[i]="quatre-vingt-"+listed[int(nombre[i+1])]+" mille"
            
            
        else :
            for i in range (n-4,n-3):
                p[i]=listeu[int(nombre[i])]
                if (p[i]==" "):
                    p[i]=listeu[int(nombre[i+1])]
                elif (p[i]=="un"):
                    p[i]="-et-"+listeu[int(nombre[i])]+" mille"
                else :    
                    p[i]=listeu[int(nombre[i])]+" mille"
        

    for i in range (n-3,n-2):
            p[i]=listeu[int(nombre[i])]
            if (p[i]=="un"):
                p[i]="cent"
            else :
                p[i]=listeu[int(nombre[i])]+" cents"
    for i in range (n-2,n-1):
            p[i]=listedec[int(nombre[i])]
            if (p[i]=="dix"):
                p[i]=listed[int(nombre[i])]
                p[i+1]=""
            elif (p[i]=="soixante-dix"):
                if ((i+1)==1):
                    p[i]="soixante-"
                    p[i+1]="-et-"+listed[int(nombre[1])]
                else :
                    p[i]="soixante-"+listed[int(nombre[i+1])]
                    p[i+1]=""
            elif (p[i]=="quatre-vingt-dix"):
                if ((i+1)==1):
                    p[i]="quatre-vingt-et-"+listed[int(nombre[i])]
                else :
                    p[i]="quatre-vingt-"+listed[int(nombre[i+1])]
            else :
                for i in range (n-1,n):
                    p[i]=listeu[int(nombre[i])]
                    if (p[i]==" "):
                        p[i]=listeu[int(nombre[i])]+" "
                    if (p[i]=="un"):
                        p[i]="-et-"+listeu[int(nombre[i])]
                    else :    
                        p[i]=listeu[int(nombre[i])]

    
    print(nombre)
    print(p)
    p.append("euros") 
    se=(" ".join(p))
    print (se)
##Programme 5 - Tkinter    
    encar = Tk()
    canevas1 = Canvas(encar,bg='gray',height=600, width=600)
    canevas1.pack()

    monImage=PhotoImage(file = 'cheque.gif')
    item = canevas1.create_image(300,300,image = monImage)

    rt=datetime.now().strftime("%d/%m/%y")
    
    bouton1=Button(encar,command=(canevas1.create_text(225,255,text=se,font="Arial 8 bold", fill="black")))
    bouton2=Button(encar,command=(canevas1.create_text(475,266,text=nombre,font="Arial 14 italic", fill="black")))
    bouton3=Button(encar,command=(canevas1.create_text(475,291,text="Eragny",font="Arial 11 italic", fill="black")))
    bouton4=Button(encar,command=(canevas1.create_text(475,311,text=rt,font="Arial 11 italic", fill="black")))
    encar.mainloop()


##Affichage du menu
def listeprogs():
    print ("1 - Le compte est bon (Des Chiffres et des Lettres)")
    print ("2 - Le triangle de Pascal")
    print ("3 - Chiffres arabes / Chiffres romains")
    print ("4 - Comptage de caractères dans une phrase")
    print ("5 - Ecrture d'un cheque")

################################################ LISTES ######################################################################################################

L=[]
L2=[]
symb=['I','V','X','L','C','D','M','V*']
listeu=[" ","un","deux","trois","quatre","cinq","six","sept","huit","neuf"]
listed=["dix","onze","douze","treize","quatorze","quinze","seize","dix-sept","dix-huit","dix-neuf"]
listedec=[" ","dix","vingt","trente","quarante","cinquante","soixante","soixante-dix","quatre-vingt","quatre-vingt-dix"]

################################################ PROGRAMME PRINCIPAL ##########################################################################################
listeprogs()

menu=int(input("Quel est le programme que vous voulez éxécuter ?"))

if (menu==1):
    flag=True
    while (flag==True):
        print("Bienvenue dans le compte est bon !")
        print(time.strftime("%A %d %B %Y %H:%M:%S"))
        Result=nbalea()
        B=randint(100,1000)
        process(Result,B)
        rep=input("Souhaitez-vous continuer (Oui/Non)")
        if (rep=="Non"):
            flag=False
            quit()

            
if (menu==2):
    flag=True
    while (flag==True):
        n=int(input("Veuillez entrer le degré : "))
        po=TrianglePascal(n)
        print ("Le triangle de pascal correspondant au degré",n,"est",po)
        print("L'expression associée est : ",expression(po,n))
        rep=input("Souhaitez-vous continuer (Oui/Non)")
        if (rep=="Non"):
            flag=False
            quit()
    
if (menu==3):
    flag=True
    while (flag==True):
        encar = Tk()
        canevas1 = Canvas(encar,bg='gray',height=300, width=400)
        canevas1.pack()
        mon_image = PhotoImage(file = 'manuscript1.gif')
        item = canevas1.create_image(200,150,image = mon_image)
        entree = Entry(encar, textvariable="", width=30)
        entree.pack(side='left')
        bouton1 = Button(encar, text= 'Rédiger', command = reponse)
        bouton1.pack(side='right')
        encar.mainloop()
        rep=input("Souhaitez-vous continuer (Oui/Non)")
        if (rep=="Non"):
            flag=False
            quit()


if (menu==4) :
    flag=True
    while (flag==True):
        chaine=input("Entrez la phrase : ")
        caract=input("quel caractère voulez-vous compter dans cette phrase? ")
        print (u"le caractère ", caract," apparait ",occurence_dans_chaine(chaine,caract), ' fois dans cette phrase.')
        rep=input("Souhaitez-vous continuer (Oui/Non)")
        if (rep=="Non"):
            flag=False
            quit()



if (menu==5) :
    flag=True
    while (flag==True):
        a=input("Entrez un nombre : ")
        convNombre(a)
        rep=input("Souhaitez-vous continuer (Oui/Non)")
        if (rep=="Non"):
            flag=False
            quit()
        


