package tdresthautil;

import com.fasterxml.jackson.databind.ObjectMapper;
import tdresthautil.Users;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/users")
public  class UserService {
    ArrayList<Users> users = new ArrayList<Users>();


    public UserService(){
        users.add(new Users("Manu", 73));
        users.add(new Users("Clement", 20));
        users.add(new Users("Lucas", 25));
        users.add(new Users("Sofiane", 24));
        users.add(new Users("Lola", 19));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getUsers() {
        //Serialiser d'objets en JSON
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        try {
            jsonInString = mapper.writeValueAsString(users);
        } catch (Exception e) {
            return e.getMessage();
        }
        return jsonInString;
    }

    @GET
    @Path("/{nom}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getUser(@PathParam("nom") String nom) {
        Users res=null;
        ObjectMapper mapper = new ObjectMapper();

        for(Users u : users)
        {
            if(u.getNom().equalsIgnoreCase(nom)) res=u;
        }
        String jsonInString = null;
        try {
            jsonInString = mapper.writeValueAsString(res);
        } catch (Exception e) {
            return e.getMessage();
        }
        return jsonInString;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String addUser(@FormParam("nom") String nom, @FormParam("age") int age) {
        Users res=null;
        ObjectMapper mapper = new ObjectMapper();

        Users u = new Users(nom, age);
        users.add(u);
        String jsonInString = null;
        try {
            jsonInString = mapper.writeValueAsString(users);
        } catch (Exception e) {
            return e.getMessage();
        }
        return jsonInString;
    }

    @DELETE
    @Path("/{nom}")
    @Produces(MediaType.APPLICATION_JSON)
    public String suppUser(@PathParam("nom") String nom) {
        Users res=null;
        ObjectMapper mapper = new ObjectMapper();
        for(Users u : users)
        {
            if(u.getNom().equalsIgnoreCase(nom)) res=u;
        }

        users.remove(res);
        String jsonInString = null;
        try {
            jsonInString = mapper.writeValueAsString(users);
        } catch (Exception e) {
            return e.getMessage();
        }
        return jsonInString;
    }



}
