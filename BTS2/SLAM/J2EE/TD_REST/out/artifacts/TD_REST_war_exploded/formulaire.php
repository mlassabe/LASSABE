<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Formulaire</title>
    <script src="jquery.js"></script>
</head>
<body>
    <label>Utilisateur a supprimer : </label>
    <input type="text" name="nom" id="user" ><br>
    <button id="toto">Supprimer</button>

    <div id="result"></div>
    <script type="text/javascript">
        $("#toto").click(function () {
            var user= $('#user').val();
            $.ajax({
                    type:"DELETE",
                    url:"/rest/users/"+user,
                    success: function(data){
                        var text="";
                        for (var i=0; i<data.length ; i++){
                            text=text +data[i].nom+","+data[i].age+","+ "ans<br>";
                        }
                        $("#result").html(text);
                    }

                }

            )
        })
    </script>

    <label>Nom :</label>
    <input type="text" name="nom" ><br>
    <p></p>
    <label>Age :</label>
    <input type="text" name="age" ><br>
    <button>Envoyer</button>
</form>
</body>
</html>
