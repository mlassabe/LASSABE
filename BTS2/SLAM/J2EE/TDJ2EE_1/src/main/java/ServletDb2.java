import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@WebServlet(name = "ServletDb2")
public class ServletDb2 extends HttpServlet {

    @Override
    public void init() throws ServletException {
        // Servlet initialization code here
        super.init();
    }

    @Override
    public void destroy() {
        // resource release
        super.destroy();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Set response content type
        response.setContentType("text/html");

        //creation de l'objet utilisé pour l'affichage
        PrintWriter out = response.getWriter();

        //affichage d'un message
        out.println("<h1>Servlet is Working!!</h1>");

        //code html formulaire
        out.println("<form method='POST' action=''>" +
                "nom: <input name=nom type='text' id='nom'>" +
                "<INPUT type=submit value=Envoyer>" +
                "</form>");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.getContentType();

        PrintWriter out = response.getWriter();

        //récupération du paramètre du formulaire et affichage
        String nom = request.getParameter("nom");

        try {

            //paramètres nécessaires à la connexion
            Class.forName("com.mysql.jdbc.Driver");

            System.out.println("Driver O.K.");

            String url = "jdbc:mysql://localhost:3306";

            String user = "root";

            String passwd = "root";

            //connexion à la database
            Connection conn = DriverManager.getConnection(url, user, passwd);

            System.out.println("Connexion effective !");

            Statement statement = conn.createStatement();

            //création de la requête
            try (ResultSet resultat = statement.executeQuery("SELECT * FROM testjdbc.users")) {

                while (resultat.next()) {
                    int age = resultat.getInt("age");
                    out.println(age);
                }
            }

            conn.close();
            out.close();

        }catch(Exception e){

            e.printStackTrace();

        }
    }

}