import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "HTTP_Servlet")
public class HTTP_Servlet extends javax.servlet.http.HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter test = response.getWriter();
        response.getContentType();
        String a = request.getParameter("line");
        test.println("Hello " + a);
        Integer s = Integer.parseInt(request.getParameter("age"));
        test.println("Tu as " + s +" ans");
        if (s<18){
            test.println("Tu es mineur");
        }else{
            test.println("Tu es majeur");
        }




        test.println("Bienvenue dans le BTS");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter test = response.getWriter();
        test.println("test \n");
        test.println("<form method='POST'>");
        test.println("<label for='text'>Quel est votre nom ? <span class='requis'></span></label>");
        test.println("<input type='text' name='line'/>");
        test.println("</br>");
        test.println("<label for='text'>Quel est votre âge ? <span class='requis'></span></label>");
        test.println("<input type='number' name='age'/>");
        test.println("</br>");
        test.println("<input type='submit' value='Hello'>");
        test.println("</form>");
    }
}
