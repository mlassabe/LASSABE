import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet(name = "ServletDb")
public class ServletDB extends HttpServlet {

    @Override
    public void init() throws ServletException {
        // Servlet initialization code here
        super.init();
    }

    @Override
    public void destroy() {
        // resource release
        super.destroy();
    }



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.getContentType();

        PrintWriter out = response.getWriter();

        //récupération du paramètre du formulaire et affichage
        String nom = request.getParameter("nom");

        //récupération de l'âge
        Integer age = Integer.parseInt(request.getParameter("age"));

        //récupération du paramètre du formulaire et affichage
        String sexe = request.getParameter("Sexe");

        //affichage d'un message
        out.println("<h1>Bienvenue!!</h1>");
        PrintWriter test = response.getWriter();
        response.getContentType();



        try {

            Class.forName("com.mysql.jdbc.Driver");

            System.out.println("Driver O.K.");

            String url = "jdbc:mysql://localhost:3306";

            String user = "root";

            String passwd = "";


            Connection conn = DriverManager.getConnection(url, user, passwd);

            System.out.println("Connexion effective !");

            Statement statement = conn.createStatement();

            statement.executeUpdate("INSERT INTO testjdbc.users VALUES ('" + nom + "', '" + sexe + "','" + age + "')");
            //autre façon changer les variables avec des ?
            //PreparedStatement stat = Conn.PrepareStatement(requete);
            //stat.setString(1, "chaine de caractère");
            //stat.setInteger(2,18);
            //attention on commence par un 1 pour les requêtes pas par un 0

        }catch(Exception e){

            e.printStackTrace();

        }

    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Set response content type
        response.setContentType("text/html");

        //creation de l'objet utilisé pour l'affichage
        PrintWriter out = response.getWriter();

        //affichage d'un message
        out.println("<h1>Bienvenue!!</h1>");

        Connection conn;




        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306";
            String user = "root";
            String passwd = "";
            System.out.println("Connexion effective !");
            Connection conn2 = DriverManager.getConnection(url, user, passwd);
            Statement statement2 = conn2.createStatement();
            ResultSet result2=statement2.executeQuery("SELECT * FROM testjdbc.users");
            int a=1;
            while (result2.next()) {
                int age = result2.getInt("age");
                String nom= result2.getString("nom");
                String sexe = result2.getString("Sexe");
                out.println(a+" Nom : "+nom);
                if (sexe == "F"){
                    out.println(" et elle a "+ age + " ans "+"<br/>");
                }else{
                    out.println(" et il a "+ age + " ans "+"<br/>");
                }
                a++;
            }
            conn2.close();
            out.close();
        }catch(Exception e){

            e.printStackTrace();

        }

    }
}