####LASSABE Emmanuel    
#####8 Janvier 2017   
###TP Wireshark (SI 56)

###En utilisant Wireshark après avoir fait une recherche sur un navigateur on obtient une ligne de type:

*165	649.46154799	10.0.17.40	10.0.2.2	HTTP	432	GET ftp://ftp.uconn.edu/ HTTP/1.1*

 Si l'on interprète, le destinataire de la trame est 10.0.2.2, l'expéditeur de cette trame est 10.0.17.40 *(moi)* et le protocole d'envoi de la trame est un protocole *HTTP* .
  
#####Voici la réponse :  
*171	649.99547829  10.0.2.2   10.0.17.40	HTTP	71	HTTP/1.1 200 OK  (text/html)*   
Si l'on interprète, le destinataire de la trame est donc 10.0.17.40 *(moi)*, l'expéditeur de cette trame est 10.0.2.2 et le protocole d'envoi de la trame est un protocole *HTTP* comme la trame de recherche HTTP.

####Via un client *FTP*, on obtient cette ligne:
*526	524.742068747	10.0.2.2	10.0.17.40	TCP	71	[TCP Retransmission] 3128 → 49444 [PSH, ACK] Seq=1888 Ack=367 Win=30080 Len=5 TSval=2745565192 TSecr=945746*  
  
#####Avec la réponse de notre client FTP qui s'en suit rapidement :  
*528	524.742176528	10.0.17.40	10.0.2.2	TCP	78	[TCP Dup ACK 3715#1] 49444 → 3128 [ACK] Seq=367 Ack=1893 Win=33280 Len=0 TSval=946128 TSecr=2745565192 SLE=1888 SRE=1893*  
  
####Pour la recherche via le *FTP* avec un password:  
*853	153.328404645	10.0.17.173	10.0.2.2	HTTP	461	GET ftp://ftp.dlptest.com/ HTTP/1.1*  

#####avec la réponse qui s'en suit évidemment:  
*854	153.334037512	10.0.2.2	10.0.17.40	TCP	66	3128 → 49448 [ACK] Seq=15735 Ack=1814 Win=34432 Len=3 TSval=2745690829 TSecr=1071709*
