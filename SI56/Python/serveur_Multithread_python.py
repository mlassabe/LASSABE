#Manu LASSABE



import socket 
import threading



class clientThread(threading.Thread): 
    
    def init( self, ip, port, clientsocket): 
        threading.Thread.init( self)
        self.ip = ip
        self.port = port 
        self.clientsocket = clientsocket
        print( "{} {} " .format(self.ip, self.port, ))

    def po( self): 
        while True : 
            print( "Connection : {} {}".format(self.ip, self.port, ))
            pi = self.clientsocket.recv(2048).decode()
            print( "Ouverture fichier: ", pi)
            fp = open(pi, 'rb')
            contenu = fp.read()
            if contenu == 'exit':
                break
            print(contenu)
            self.clientsocket.send(contenu)
            print( "Client déconnecté...")


TCP_IP = 'localhost'
TCP_PORT = 1111
BUFFER_SIZE = 20

tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((TCP_IP, TCP_PORT))
threads = []

while True:
    tcpsock.listen(10)
    print("En écoute...")
    (clientsocket, (ip, port))= tcpsock.accept()
    newthread = ClientThread(ip, port, clientsocket)
    newthread.start()
    threads.append(newthread)

print("Close")
clientsocket.close()
socket.close()
