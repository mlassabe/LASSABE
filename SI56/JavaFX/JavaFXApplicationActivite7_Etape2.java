/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplicationactivite7;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;;  
         
public class JavaFXApplicationActivite7 extends Application { 
   @Override 
   public void start(Stage stage) {      
      
       VBox root=new VBox(0);

      //Creation du label nombre
      Text text1 = new Text("1er nombre : ");      
      Text text2 = new Text("2eme nombre : "); 
       
      //Creation Text Field pour      
      TextField textField1 = new TextField();       
      TextField textField2 = new TextField();
      
       
      
      
      //Creation des boutons
      Button button1 = new Button("Multiplier"); 
      Button button2 = new Button("Diviser"); 
      Button button3 = new Button("Additionner");
      Button button4 = new Button("Soustraire");
      
      //Creation de la grille GridPane 
      GridPane grid = new GridPane();
      GridPane grid1 = new GridPane();
      GridPane grid2= new GridPane();
      
      //Setting size for the pane 
      grid.setMinSize(500, 50); 
      grid1.setMinSize(500,50);
      grid2.setMinSize(250, 50);
      
      //Setting the padding  
      grid.setPadding(new Insets(5,5,5,5));
      grid1.setPadding(new Insets(10,10,10,10));
      grid2.setPadding(new Insets(5,5,5,5));
      
      
      //Setting the vertical and horizontal gaps between the columns 
      grid.setVgap(5); 
      grid.setHgap(80);
      grid1.setHgap(5);
      grid1.setHgap(5);
      
      //Setting the Grid alignment 
      grid.setAlignment(Pos.CENTER);
      grid1.setAlignment(Pos.CENTER);
      grid2.setAlignment(Pos.CENTER);
      //grid.setGridLinesVisible(true);
      //grid1.setGridLinesVisible(true);
     
      //Affiliation à Vbox
      root.getChildren().add(grid);
      root.getChildren().add(grid1);
      root.getChildren().add(grid2);
      //Creation des HBtn
      HBox hbBtn = new HBox(10); //Taille du bouton
      hbBtn.setAlignment(Pos.BASELINE_LEFT); //Emplacement du bouton
      hbBtn.getChildren().add(button1);
      hbBtn.getChildren().add(button2);
      HBox hbBtn1 = new HBox(10); //Taille du bouton
      hbBtn1.setAlignment(Pos.BASELINE_RIGHT); //Emplacement du bouton
      hbBtn1.getChildren().add(button3);
      hbBtn1.getChildren().add(button4);
      
      
      //Alignement des TextField 
      
      textField1.setAlignment(Pos.BASELINE_LEFT);
      textField2.setAlignment(Pos.BASELINE_LEFT);
      
      
      
      
      //Style des boutons  
      button1.setStyle("-fx-background-color: lightgreen; -fx-text-fill: black; -fx-border-color:black;"); 
      button2.setStyle("-fx-background-color: lightgreen; -fx-text-fill: black; -fx-border-color:black;"); 
      button3.setStyle("-fx-background-color: lightgreen; -fx-text-fill: black; -fx-border-color:black;");
      button4.setStyle("-fx-background-color: lightgreen; -fx-text-fill: black; -fx-border-color:black;"); 
      
      
      //Style 
      text1.setStyle("-fx-font: normal  14px 'Arial' "); 
      text2.setStyle("-fx-font: normal  14px 'Arial' ");  
      grid.setStyle("-fx-background-color: CORNFLOWERBLUE;"); 
      grid1.setStyle("-fx-background-color: CORNFLOWERBLUE;"); 
      grid2.setStyle("-fx-background-color: CORNFLOWERBLUE;"); 
      
      //Création du label de réponse 
      Label reponse=new Label();
      reponse.setStyle("-fx-background-color: cornflowerblue; -fx-text-fill: black; -fx-font: bold 18px 'Arial'; -fx-border-color:royalblue; "); 
      reponse.setAlignment(Pos.BOTTOM_CENTER);
      grid2.getChildren().add(reponse);
      //Creation Action Events 
      
      //Création des actions event correspondant aux actions sur la pression des boutons
      final Text Action1=new Text();
      button1.setOnAction(new EventHandler<ActionEvent>(){
          public void handle(ActionEvent e){
              double nb1;double nb2;double resultat;
              nb1=Double.parseDouble(textField1.getText());
              nb2=Double.parseDouble(textField2.getText());
              resultat=nb1*nb2;
              reponse.setText(String.valueOf(resultat));
              
   }});
      final Text Action2=new Text();
      button2.setOnAction(new EventHandler<ActionEvent>(){
          public void handle(ActionEvent e){
              double nb1;double nb2;double resultat;
              nb1=Double.parseDouble(textField1.getText());
              nb2=Double.parseDouble(textField2.getText());
              resultat=nb1/nb2;
              reponse.setText(String.valueOf(resultat));
          }
   });
     final Text Action3=new Text();
    button3.setOnAction(new EventHandler<ActionEvent>(){
          public void handle(ActionEvent e){
              double nb1;double nb2;double resultat;
              nb1=Double.parseDouble(textField1.getText());
              nb2=Double.parseDouble(textField2.getText());
              resultat=nb1+nb2;
              reponse.setText(String.valueOf(resultat));
          }
   }); 
    final Text Action4=new Text();
      button4.setOnAction(new EventHandler<ActionEvent>(){
          public void handle(ActionEvent e){
              double nb1;double nb2;double resultat;
              nb1=Double.parseDouble(textField1.getText());
              nb2=Double.parseDouble(textField2.getText());
              resultat=nb1-nb2;
              reponse.setText(String.valueOf(resultat));
          }
   });
      
      
      
      //Arrangement des éléments sur la grille
      grid.add(text1, 0, 0); 
      grid.add(textField1, 1, 0); 
      grid.add(text2, 0, 1);       
      grid.add(textField2, 1, 1); 
      grid1.add(hbBtn, 0, 0); 
      grid1.add(hbBtn1, 1, 0);
      grid2.add(Action1,0,0);
      
      //Création de la scene
      Scene scene = new Scene(root); 
      
      
      //Nommage de la fenêtre
      stage.setTitle("Application Calcul"); 
         
      //Mise en place de la scene sur la fenetre 
      stage.setScene(scene);
      
      //Lancement de la fenêtre
      stage.show(); 
   }      
   public static void main(String args[]){ 
      launch(args); 
   } 
}