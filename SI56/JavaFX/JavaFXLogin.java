/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author elass
 */
public class JavaFXApplication1 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
// Set Window's Title
        primaryStage.setTitle("Login / ");
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        Text scenetitle = new Text("MLProd by Manu Lassabe");
        scenetitle.setFont(Font.font("Caviar Dreams", FontWeight.BOLD, 20)); //Titre du formulaire avec ses caractéristiques
        grid.add(scenetitle, 0, 0, 2, 1); //Emplacement sur l'encar
        Label userName = new Label( "User Name:" ); //Titre du 1er formulaire
        userName.setFont(Font.font("Caviar Dreams", FontWeight.NORMAL, 14)); //Caracteristiques du 1er formulaire
        grid.add(userName, 0, 1); //Emplacement sur l'encar
        TextField userTextField = new TextField(); //Nouveau formulaire de saisie
        grid.add(userTextField, 1, 1); //Emplacement
        Label pw = new Label("Password:"); //Titre
        pw.setFont(Font.font("Caviar Dreams", FontWeight.NORMAL, 14)); //Caracteristiques (Police, Taille,Stylistique)
        grid.add(pw, 0, 2); //Emplacement
        PasswordField pwBox = new PasswordField(); //Nouvel endroit de saisie 
        grid.add(pwBox, 1, 2); //Emplacement de la saisie
        Button btn = new Button( "Entrez" ); //Bouton 
        HBox hbBtn = new HBox(10); //Taille du bouton
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT); //Emplacement du bouton
        hbBtn.getChildren().add(btn); //
        grid.add(hbBtn, 1, 4);
        Button btn1 = new Button( "Vous identifier" );
        HBox hbBtn1 = new HBox(20);
        hbBtn1.setAlignment(Pos.BOTTOM_LEFT);
        hbBtn1.getChildren().add(btn);
        grid.add(hbBtn1, 1, 4);
        final Text actiontarget = new Text();
        grid.add(actiontarget, 1, 6);
        scenetitle.setFill(Color.ROYALBLUE);//Couleurs 
        userName.setTextFill(Color.BLUE);//Couleurs
        pw.setTextFill(Color.RED);//Couleurs
        btn.setOnAction( new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent e) {
            actiontarget.setFill(Color.BLUE);
            actiontarget.setId( "actiontarget" );
            actiontarget.setText( "Merci de votre identification" );
        }
       });
    Scene scene = new Scene(grid, 600, 300);
    primaryStage.setScene(scene);
    primaryStage.show();
}

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args); //LAncement de l'encar
    }
    
}