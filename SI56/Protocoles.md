###  TP Protocoles
#### Emmanuel LASSABE
#### Décembre 2016
#### BTS1 - SLAM
----
Les protocoles 
----
###### *HTTP*  
*Ce protocole est un protocole client-serveur développé pour le World Wide Web (www)*
*Le serveur Web appelé par le client qui sotn les navigateurs comme Mozilla Firefox, Google Chrome ou même Microsoft Edge appellent des pages au serveur comme Apache qui vous les donnent*
*Il existe cependant des intermédiaires comme une passerelle, un proxy, et un VPN*
*Il existe differents types de méthodes :*

----

*- GET : Méthode la plus courante pour demander une ressource*
*- HEAD : Méthode qui ne demande que des informations sur la ressource*
*- POST : Méthode qui transmet des données pour un traitement à la ressource*
*- OPTIONS : Obtention d'une communication d'une ressource ou d'un serveur en général*
*- CONNECT : Proxy comme tunnel de communication*
*- TRACE : Retour de ce que le serveur a reçu, dans le but d'un diagnostic de communication*
*- PUT : Ajouter ou remplacer une ressource sur un serveur*
*- PATCH : Modification partielle d'une ressource*
*- DELETE  : Suppression d'une ressource du serveur*

----

###### *SMTP*
*Ce protocole sert à l'envoi et à la réception des courriels*  
*Il va récupérer le mail, l'envoyer par relais SMTP, et le déposer dans le serveur SMTP et la boîte du destinataire* 
*Ces requêtes se composent des plusieurs composantes de votre mail, le destinataire, l'expéditeur, le sujet du message et le corps du texte*  

---- 

*Le serveur SMTP répond quant à lui par des codes tels que :*
*- 220 Premier code envoyé par le serveur lorsque la connexion s'est effectuée avec succès.*
*- 250 Confirmation de commande acceptée*
*- 354 Réponse à la commande DATA*
*- 421 Erreur temporaire au niveau de la connection.*
*- 452 Echec temporaire*
*- 550 Echec permanent. La boite mail n'existe pas*
*- 554 Echec permanent au niveau de la connection*
*Les clients sont les boites mails et les serveurs*

----

###### *FTP*  
*le protocole FTP est un protocole de communication de partage de fichiers pour un réseau TCP/IP*
*Il permet l'ajout, la modification ou la suppression de fichiers. Il est utilisé pour les sites Web hébergés chez des particuliers*
*Pour accéder à un serveur FTP comme FileZilla , on utilise un logiciel (client FTP : SmartFTP). Le standard FTP est très répandu car de très nombreux logiciels sont inclus avec les dernières versions de distributions Windows & Linux.*

----

*Le protocole utilise deux types de connexions TCP :
Une connexion de contrôle initialisée par le client, vers le serveur pour transmettre les commandes de fichiers (modification, ajout ou suppression).
Et une connexion de données initialisée par le client ou le serveur pour transférer les données requises (contenu ou liste de fichiers).*

----

###### *IRC*
*Internet Relay Chat ou IRC est un protocole de communication textuelle sur Internet. Il sert à la communication instantanée principalement sous la forme de discussions en groupe par l'intermédiaire de canaux de discussion, mais peut aussi être utilisé pour de la communication de un à un. Il peut par ailleurs être utilisé pour faire du transfert de fichier.*

----

*Le protocole de communication décrit un réseau informatique formé de plusieurs serveurs connectés comme UnrealIRCd sans boucle dans lequel les clients comme HexChat communiquent généralement par le biais du serveur. Il est également possible de connecter deux clients directement pour une conversation privée ou un transfert de fichier, c'est le DCC. Ce protocole étant public, des clients existent pour de nombreux systèmes d'exploitation, de même que les serveurs IRC, aussi désignés par le terme IRCD qui signifie Internet Relay Chat Daemon.*

----

*L'élément de base pour communiquer sur un réseau est le canal. Un canal est défini par une liste d'utilisateurs connectés sur celui-ci. Pour entrer ou pour créer un canal s'il n'existe pas, l'utilisateur utilise la commande "join". Les canaux peuvent être vus de tous, leur nom est alors préfixé par le caractère croisillon " # " ;
ils peuvent aussi être locaux, portés par un serveur uniquement, leur nom est dans ce cas préfixé par le caractère esperluette " & ".*






