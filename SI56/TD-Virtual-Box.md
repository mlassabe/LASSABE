# TD Virtual Box  
## LASSABE Emmanuel
### 30/01/2017

### VirtualBox
### Mode d'emploi

----

### Création d'une machine virtuelle avec VirtualBox

* Créer une nouvelle machine virtuelle lorsque vous êtes sur l'interface 
* Configurer la machine avec l'OS (Windows, Linux) et la taille de la mémoire alloué à la machine virtuelle
* Créer un disque dur virtuel
* La taille du fichier correspond au giga que l'on veut allouer pour notre machine virtuelle.
* Dans le type de disque dur selectionner VDI de préférence
* On selectionne pour un stockage le dynamique alloué qui permet au disque de grossir au fur et à mesure à chaque donnée insérée sur le disque jusqu'à atteindre la taille maximale.
* Pour chercher l'ISO, allez 
* Dans les paramètres, on va dans stockage. nous voyons une disquette, on clique desuss et allons chercher notre iso.
* Lancer la virtualbox

----

### Installation de l'OS sur VirtualBox 

* Dans le menu d'installation de Debian. Choisir graphical install pour une meilleure adaptation. 
* Dans les réglages,chosir la langue, le pays ainsi que le clavier. Pour notre cas, le français.
* Choisir le nom de notre machine, puis le nom de domaine. Ici, on le laisse car il nous est connu.
* Dans le choix du mot de passe root, ne mettez rien. 
* Arrivée dans le partitionnement, comme on est sur virtual box on assigne une partie du disque car on est sur machine virtuelle. Ensuite on met tout dans la même partition.
* Télécharger les paquets.

----

#### Partage avec la machine physique 

* Créer un dossier partagé. Pour cela, il faut avoir insérer l'image cd  dans le menu périphérique et l'installer.
* Ensuite aller dans l'onglet "Configuration" dans le menu des Virtuals Box. On indique le chemin du dossier à partager et il apparaitra.

