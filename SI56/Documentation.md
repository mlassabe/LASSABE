##Documentation gitLab
**Emmanuel LASSABE **  
BTS1 SLAM  
Decembre 2016  

==

présentation de git:

        
	GitLAB est un logiciel créé par Linux Tovalds. 
	
	Git est compose de deux structures de donnees: une base de donnees et un cache de repertoire. 
	Il y a 4 type d'objets :  Blob Tree Commit et Tag.



---
quelques commandes:

        
	git config --global user.name "VotreUserName"
        
	git config --global user.email "Votre@email.fr" 
	
	Permettant de se connecter à GitLab
        
	git clone permettant de cloner des fichiers
        
	git init pour initialiser nos données 
        
	git add pour ajouter un fichier à notre stockage de données
       
	git commit -m pour modifier un fichier 
        
	git status permet de connaitre le repertoire actuel sur lequel on se trouve
        
	git push origine master pour "envoyer" les changements faits sur le fichier



---
sources:

        
	https://aide.lws.fr/base/Serveurs-dedies/Serveur-GIT/Comment-travailler-avec-Git-et-interface-GitLab
        
	https://fr.wikipedia.org/wiki/Git