import java.util.Scanner;
public class Application {
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Quel programme voulez-vous choisir ? ");
		System.out.println("1 ) Affiche votre saisie");
		System.out.println("2 ) Affiche le produit de 2 nombres");
		System.out.println("3 ) Parité d'un nombre");
		System.out.println("4 ) Table de multiplication");
		System.out.println("5 ) Catégorie Basket");
		System.out.println("6 ) Décrémentation");
		System.out.println("7 ) Somme des entiers");
		System.out.println("8 ) Tableau / Mini et Maxi");
		int nombre=sc.nextInt();
		Bibliotheque bib=new Bibliotheque();
		if (nombre==1){
			bib.bonjour();
		}
		if (nombre==2){
			System.out.println("1er nombre :");
			int nb1=sc.nextInt();
			System.out.println("2eme nombre :");
			int nb2=sc.nextInt();
			bib.produit(nb1,nb2);
			
		}
		if (nombre==3){
			bib.parite();
		}
		if (nombre==4){
			System.out.print("Veuillez entrer un nombre à multiplier: ");
			int nombre1 = sc.nextInt();
			System.out.print("Veuillez entrer le nombre de fois à multiplier: ");
			int multi = sc.nextInt();
			bib.multiplication(nombre1,multi);
		}
		if (nombre==5){
			System.out.print("Veuillez entrer un âge : ");
			int nombre1 = sc.nextInt();
			bib.categorie(nombre1);
		}
		if (nombre==6){
			System.out.print("Veuillez entrer un nombre à décrémenter : ");
			int nombre1 = sc.nextInt();
			bib.decrementation(nombre1);
		}
		if (nombre==7){
			System.out.print("Veuillez entrer un nombre : ");
			int nombre1 = sc.nextInt();
			bib.sommeEntiers(nombre1);
		}
		if (nombre==8){
			System.out.print("Veuillez entrer le nombre d'entrées au Tableau :");
			int nombre1 = sc.nextInt();
			bib.Tableau(nombre1);
		}
	
	}
}
